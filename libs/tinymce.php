<?php

	global $lg_tinymce_custom;
	
	$lg_tinymce_custom = array(
	    'title' => 'Custom',
	    'items' =>  array(
	    	array(
				'title' => 'White Bullet',
	            'selector' => 'ul',
	            'classes' => 'white-bullet'
			),
			array(
				'title' => 'No Bullet',
	            'selector' => 'ul',
	            'classes' => 'no-bullet'
			),
			array(
				'title' => 'Extra Large Title',
	            'selector' => '.h1',
	            'classes' => 'h1-xl'
			)
	    )
	);

?>