<div class="utility-bar">
  <div class="d-flex justify-content-between">
    <div class="left">
      <?php do_action('wp_utility_left'); ?>
    </div>

    <div class="center">
      <?php do_action('wp_utility_center'); ?>
    </div>

    <div class="right">
      <?php do_action('wp_utility_right'); ?>
    </div>
  </div>
</div>