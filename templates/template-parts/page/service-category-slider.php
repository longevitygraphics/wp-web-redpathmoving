<?php
	$terms = get_terms('service-category', array(
	    'hide_empty' => false,
	));
?>

<div class="service-category-slider-wrap">
	<?php if($terms && is_array($terms)): ?>
		<div class="service-category-slider px-5 px-md-5">
		<?php foreach ($terms as $key => $term):
			$image = get_field('feature_image', $term);
			$intro_copy = get_field('short_descriotion', $term);
		?>
		
			<div class="single-service-category-slider">
				<div class="row">
					<div class="col-md-6">
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
					</div>
					<div class="col-md-6 pt-4 pt-md-0">
						<h2 class="mb-3 text-dark"><?php echo $term->name; ?></h2>
						<?php echo $intro_copy; ?>
						<a href="<?php echo get_term_link($term); ?>" class="btn btn-primary mt-4">Learn More</a>
					</div>
				</div>
			</div>
			
		<?php endforeach; ?>
		</div>
	<?php endif; ?>
</div>

<script>
	(function($) {
	
    $(document).ready(function(){
		$('.service-category-slider').slick({
		  	arrows: true,
		  	autoplay: true,
			autoplaySpeed: 5000
		});
    });

}(jQuery));
</script>