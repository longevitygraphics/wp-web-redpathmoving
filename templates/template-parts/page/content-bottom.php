<?php if (have_rows('involvement_slider', 'option')): ?>
	<div class="container bg-light py-5">
		<h2 class="h1 text-dark font-weight-normal mb-5 involvement_title text-center">Our Involvement</h2>
		<div class="involvement_slider">
			<?php while ( have_rows('involvement_slider', 'option') ) : the_row(); ?>
				<?php
					$involvement_image = get_sub_field('involvement_image','option');
					$involvement_link = get_sub_field('involvement_link', 'option');
				?>
				<div class="involvement_slide d-flex justify-content-center flex-column align-items-center">
					<a href="<?php echo $involvement_link["url"]; ?>" target="<?php $involvement_link['target'] ?>">
						<img src="<?php echo $involvement_image["url"];?>" alt="<?php echo $involvement_image["alt"]; ?>" class="involvement_image">
						<p class="text-center pt-1 font-weight-bold"> <?php echo $involvement_link['title']; ?> </p>
					</a>
				</div>
			<?php endwhile; ?>
	</div>
<?php else: echo "No Slides" ?>
<?php endif; ?>