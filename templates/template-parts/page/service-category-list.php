<?php
	$terms = get_terms('service-category', array(
	    'hide_empty' => false,
	));
?>

<div class="service-category-list">
	<?php if($terms && is_array($terms)): ?>
		<div class="row align-items-stretch">
		<?php foreach ($terms as $key => $term):
			$icon = get_field('icon', $term);
			$intro_copy = get_field('short_descriotion', $term);
		?>
		
			<div class="col-sm-6 col-md-4 py-3">
				<div style="height: 100%;" class="px-0 px-sm-3 d-flex justify-content-between align-items-center flex-column">
					<div class="text-center">
						<img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt']; ?>">
						<h2 class="h4 my-3"><?php echo $term->name; ?></h2>
						<?php echo $intro_copy; ?>
					</div>
					<a href="<?php echo get_term_link($term); ?>" class="btn btn-sm btn-outline-primary mt-4">Learn More</a>
				</div>
			</div>
			
		<?php endforeach; ?>
		</div>
	<?php endif; ?>
</div>