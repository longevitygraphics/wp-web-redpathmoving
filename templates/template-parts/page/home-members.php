<h2 class="h1 text-dark font-weight-normal mb-4 text-center">Proud Members Of</h2>

<?php

if( have_rows('members', 'option') ): ?>
	<div class="members-list">
    <?php while ( have_rows('members', 'option') ) : the_row();
        $image = get_sub_field('image');
        $link = get_sub_field('link');
        ?>
        <a target="_blank" href="<?php echo $link; ?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"></a>
    <?php endwhile; ?>
    </div>
<?php else :
    // no rows found
endif;

?>

<script>
	// Windows Ready Handler

(function($) {

    $(window).load(function(){

        $('.members-list').slick({
          dots: true,
          arrows: false,
          autoplay: true,
          autoplaySpeed: 4000,
          speed: 600,
          cssEase: 'ease-out',
          slidesToShow: 4,
          slidesToScroll: 1,
          responsive: [
	          {
	            breakpoint: 1000,
	            settings: {
	              cssEase: 'linear',
	              slidesToShow: 4,
	            }
	          },
	          {
	            breakpoint: 768,
	            settings: {
	              cssEase: 'linear',
	              slidesToShow: 3,
	            }
	          },
	          {
	            breakpoint: 480,
	            settings: {
	              dots: false,
	              cssEase: 'linear',
	              slidesToShow: 2,
	            }
	          }
	        ]
        });
        
    });

}(jQuery));
</script>