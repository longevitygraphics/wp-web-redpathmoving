<?php

get_header(); ?>

	<?php
		$term = get_queried_object();
		$intro_content = get_field('intro_content', $term);
		$intro_2_content = get_field('intro_2_content', $term);
	?>

	<main>

		<?php flexible_layout(); ?>

		<div class="py-5 col-12 container">
			<h2 class="text-dark">Areas of Expertise</h2>
			<?php
			$posts_array = get_posts(
			    array(
			        'posts_per_page' => -1,
			        'post_type' => 'service',
			        'order' => 'ASC',
			        'tax_query' => array(
			            array(
			                'taxonomy' => 'service-category',
			                'field' => 'term_id',
			                'terms' => $term->term_id,
			            )
			        )
			    )
			);
			?>

			<?php if($posts_array && is_array($posts_array)): ?>
				<div class="service-cat-slider" id="servicecatslider">
				<?php foreach ($posts_array as $key => $post):
					$post_id = $post->ID;
					$image = get_the_post_thumbnail_url($post_id);
					$image_id = get_post_thumbnail_id($post_id);
					$alt = get_post_meta($image_id, '_wp_attachment_image_alt', true);
					$title = get_the_title($post_id);
					$short_description = get_field('short_description', $post_id);
					$post_url = get_the_permalink($post_id);
				?>

					<div class="service-cat-slide mt-3 d-flex flex-column align-items-center">
						<div class="slide-container">
						  <a href="<?php echo $post_url ?>">
						  	<img src="<?php echo $image; ?>" alt="<?php echo $alt ?>" class="slide-image">
						  </a>
						  <div class="slide-overlay">
						    <div class="slide-text"><?php echo $short_description; ?></div>
						    <a class="btn btn-outline-white" href="<?php echo $post_url ?>"><strong class="mb-2"><?php echo $title; ?></strong></a>
						  </div>
						</div>
					</div>
				<?php endforeach; ?>	
				</div>
			<?php endif; ?>				
		</div>

	</main>

<?php get_footer(); ?>