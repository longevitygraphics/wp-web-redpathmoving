<?php
/**
 * Text Block Layout
 */
?>

<?php

	get_template_part( '/layouts/partials/block-settings-start' );

?>

<!--------------------------------------------------------------------------------------------------------------------------------->

	<div class="d-flex flexible_text 
	<?php
	if ( $container == 'container-wide' ) {
		echo 'no-gutters';}
	?>
	 row <?php the_sub_field( 'align_items_vertical' ); ?> <?php the_sub_field( 'align_items_horizontal' ); ?>">
		<div class="col-12 q-and-as">
			<?php if ( have_rows( 'q_and_a' ) ) : ?>
				<?php
				while ( have_rows( 'q_and_a' ) ) :
					the_row();
					?>
					<?php
					$question = get_sub_field( 'question' );
					$answer   = get_sub_field( 'answer' );

					?>
					<div class="d-md-flex q-a align-items-start">
						<div class="questions col-md-6 d-flex align-items-center">
							<h2 class="h1 flex-shrink-0 mr-2 mb-0">Q: </h2>
                            <?php echo $question; ?>
						</div>
						
						<div class="answers col-md-6 d-flex">
							<h2 class="h1 flex-shrink-0 mr-2 mb-0">A: </h2>
                            <?php echo $answer; ?>
						</div>
					</div>
				<?php endwhile; ?>
			<?php endif; ?>
		</div>
	</div>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php

	get_template_part( '/layouts/partials/block-settings-end' );

?>
