<?php

	global $template_to_load;

	$template_to_load = array(
		'/templates/template-parts/page/service-category-list'	=>	'Service Category List',
		'/templates/template-parts/page/home-members'	=>	'Members List',
		'/templates/template-parts/page/testimonials'	=>	'Testimonials'
	);

?>