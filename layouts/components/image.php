<?php

	// $image
	$src = $image['src'];
	$settings = $image['settings'];
	$size = $settings['size'];
	$width = $settings['width'];
	$height = $settings['height'];
    $align = $settings['align'];
?>

	<?php if($size == 'img-full'): ?>
        <?php
            echo wp_get_attachment_image( $src["ID"], 'large');
        ?>	
	<?php elseif($size == 'img-fixed'): ?>
		<img class="img-fixed d-block" style="object-fit: cover; width: 100%; max-width: <?php echo $width; ?>; height: <?php echo $height; ?>; <?php if($align == 'center'): ?>margin-left: auto; margin-right: auto;<?php elseif($align == 'right'): ?>margin-left: auto;<?php endif; ?>" src="<?php echo $src['url']; ?>" alt="<?php echo $src['alt']; ?>">
	<?php endif; ?>
	